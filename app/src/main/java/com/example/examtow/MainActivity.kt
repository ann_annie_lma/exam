package com.example.examtow

import android.annotation.SuppressLint
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.graphics.green
import com.example.examtow.databinding.ActivityMainBinding
import java.util.regex.Pattern

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    val userList = mutableListOf<User>()

    private var usersDeleted = 0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        listeners()
    }


    private fun listeners()
    {
        binding.btnAdd.setOnClickListener {
            if(validateFields()) {

                addUser()
            }
        }


        binding.btnDelete.setOnClickListener {

            deleteUser()

        }


        binding.btnShow.setOnClickListener {
            showUser()
        }
    }


    private fun validateFields():Boolean
    {
        if(binding.etEmail.text.toString().isEmpty() || binding.etFirstName.text.toString().isEmpty() ||
            binding.etLastName.text.toString().isEmpty() ||
            binding.etAge.text.toString().isEmpty())
        {
            Toast.makeText(this,R.string.fill_all_fields,Toast.LENGTH_SHORT).show()
            return false



        } else if(!emailValidator())
        {
            Toast.makeText(this,R.string.invalid_email,Toast.LENGTH_SHORT).show()
            return false

        }
        return true
    }


    //to validate email
    private fun emailValidator():Boolean
    {
        val emailRegex = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
        )

        return emailRegex.matcher(binding.etEmail.text.toString()).matches()
    }


    private fun addUser()
    {
        var isUserInList = false

        for (item in userList) {
            if (item.eMail == binding.etEmail.text.toString()) {
                binding.txtNotification.setText(R.string.user_is_already_added)
                binding.txtNotification.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.red))

                isUserInList = true
                clearFields()

            }

        }

        if(!isUserInList)
        {
            userList.add(
                User(
                    binding.etFirstName.text.toString(),
                    binding.etLastName.text.toString(),
                    binding.etEmail.text.toString(),
                    binding.etAge.text.toString().toInt()
                )
            )

            binding.txtNotification.setText(R.string.added)
            binding.txtNotification.setTextColor(ContextCompat.getColor(this,R.color.green));
            binding.userAddedCount.setText(userList.size.toString())

            clearFields()


        }
    }

    private fun deleteUser()
    {
        var isUserInList = false

        userList.forEachIndexed {index,item ->
            if (item.eMail == binding.etEmail.text.toString())
            {
                isUserInList = true
                usersDeleted++
                userList.removeAt(index)
                binding.userAddedCount.setText(userList.size.toString())
                binding.userDeletedCount.setText(usersDeleted.toString())
                binding.txtNotification.setText(R.string.user_deleted)
                binding.txtNotification.setTextColor(ContextCompat.getColor(this,R.color.green));
                clearFields()

            }

        }

        if(!isUserInList)
        {
            binding.txtNotification.setText(R.string.no_user_found)
            binding.txtNotification.setTextColor(ContextCompat.getColor(this,R.color.red))

        }

    }

    private fun showUser() {

        userList.forEachIndexed { index, item ->
            if (item.eMail == binding.etEmail.text.toString()) {


                binding.etFirstName.setText(item.firstName)
                binding.etLastName.setText(item.lastName)
                binding.etAge.setText(item.age.toString())
                binding.etEmail.setText(item.eMail)


            }
        }

    }


    private fun clearFields() {
        var i = 0
        val count: Int = binding.root.getChildCount()
        while (i < count) {
            val view: View = binding.root.getChildAt(i)
            if (view is EditText) {
                (view as EditText).setText("")
            }
            ++i
        }
    }

    private fun clearNotification()
    {
        binding.txtNotification.setText("")
    }

}